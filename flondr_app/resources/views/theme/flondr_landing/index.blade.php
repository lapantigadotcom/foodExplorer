@extends('theme.flondr_landing.layout.index')

@section('content')                       
  
  @include('theme.flondr_landing.partials.menu')
   @include('theme.flondr_landing.layout.slider')
   @include('theme.flondr_landing.layout.flondr_features')
  <div class="clearfix"> </div>
    
   @include('theme.flondr_landing.layout.get_touch')
   @include('theme.flondr_landing.partials.footer')
   @include('theme.flondr_landing.partials.script')
         
 
@endsection