 
  
        <div class="text-center headlinebarFix">FEATURES
        </div>
 


<div id="fea" class="features">
  <div class="container">
    <div class="clearfix"> </div>
    <div class="features-grids">
      <div class="col-md-4 features-grid">
        <div class="features-grid-info">
          <div class="col-md-2 features-icon">
           </div>
          <div class="col-md-10 features-info">
            <h4>Bussines Places</h4>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
           <hr class="line_bottom">
          </div>
          
        </div>
        <div class="features-grid-info">
          <div class="col-md-2 features-icon">
             
          </div>
          <div class="col-md-10 features-info">
            <h4>Office Hour</h4>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
           <hr class="line_bottom">
          </div>
          
        </div>
        <div class="features-grid-info">
          <div class="col-md-2 features-icon">
           </div>
          <div class="col-md-10 features-info">
            <h4>Promotion</h4>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
          </div>
          
        </div>

      </div> 
      <div class="col-md-4 features-grid">
        <div class="big-divice">
          <img src="{{ asset('assets/theme/flondr/images/mockup_flondr-02.png')}}" style="max-width:344px" title="features-demo" />
        </div>
      </div> 
      <div class="col-md-4 features-grid">
        <div class="features-grid-info">
          <div class="col-md-2 features-icon1">
           </div>
          <div class="col-md-10 features-info">
            <h4>Write Review</h4>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
             <hr class="line_bottom">
          </div>

         
        </div>

        <div class="features-grid-info">
          <div class="col-md-2 features-icon1">
           </div>
          <div class="col-md-10 features-info">
            <h4>Get Location</h4>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
             <hr class="line_bottom">
           </div>          
        </div>
        <div class="features-grid-info">
          <div class="col-md-2 features-icon1">
           </div>
          <div class="col-md-10 features-info">
            <h4>Image Upload</h4>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
          </div>
          <div class="clearfix"> </div>
        </div>

      </div> 
      <div class="clearfix"> </div>
    </div>
  </div>
 

