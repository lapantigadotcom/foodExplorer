<div class="show-reel text-center">
              <div class="container" style="color:#fff">
                <h5><a class="popup-with-zoom-anim" href="#small-dialog"><span> </span></a> Connecting Places</h5>
                <div id="small-dialog" class="mfp-hide">
                  <iframe src="#" width="100%" height="400" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen> </iframe>
                </div>
                <script>
                $(document).ready(function() {
                  $('.popup-with-zoom-anim').magnificPopup({
                    type: 'inline',
                    fixedContentPos: false,
                    fixedBgPos: true,
                    overflowY: 'auto',
                    closeBtnInside: true,
                    preloader: false,
                    midClick: true,
                    removalDelay: 300,
                    mainClass: 'my-mfp-zoom-in'
                  });

                });
                </script> 
              </div>
            </div>