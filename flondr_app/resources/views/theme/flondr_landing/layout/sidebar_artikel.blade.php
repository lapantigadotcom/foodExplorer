         <div class="col-md-3 col-xs-12">
           
                <h5 class="widget-title line-bottom">Artikel Terbaru</h5>
          <ul class="list-unstyled list-style-1">
            @foreach($data['latest_posts'] as $row)
            <li><a href="#">          
              <a href="{!! route('home.post',[ $row->id,'post' ]) !!}"><h5>  <b> {!! substr(strip_tags($row->title),0, 20).'...' !!} </b> </h5></a>
              <p style="font-size:13px">
                {!! substr(strip_tags($row->description),0, 140).'...' !!}
              </p>
              <small>
                <i class="fa fa-calendar"></i> </i> {{ date("d M Y" ,strtotime($row->created_at)) }} &nbsp;&nbsp;&nbsp;
                <i class="fa fa-user"></i> </i> {{ $row->user->name }} &nbsp;&nbsp;&nbsp;
              </small>
              <hr>
            </li>
            @endforeach
          </ul>
         </div>

         <div class="col-md-3 col-xs-12">
           
                <h5 class="widget-title line-bottom">Artikel Terkait</h5>
          <ul class="list-unstyled list-style-1">
            @foreach($data['latest_posts'] as $row)
            <li><a href="#">          
              <a href="{!! route('home.post',[ $row->id,'post' ]) !!}"><h5>  <b> {!! substr(strip_tags($row->title),0, 20).'...' !!} </b> </h5></a>
              <p style="font-size:13px">
                {!! substr(strip_tags($row->description),0, 140).'...' !!}
              </p>
              <small>
                <i class="fa fa-calendar"></i> </i> {{ date("d M Y" ,strtotime($row->created_at)) }} &nbsp;&nbsp;&nbsp;
                <i class="fa fa-user"></i> </i> {{ $row->user->name }} &nbsp;&nbsp;&nbsp;
              </small>
              <hr>
            </li>
            @endforeach
          </ul>
         </div>
 