   
<!DOCTYPE HTML>
<html>
<head>
  <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
  <meta name="description" content=" {{ $general->meta_description }}" />
  <meta name="keywords" content=" {{ $general->meta_keywords }}" />
  <meta name="author" content=" {{ $general->about_community }}" />
  @include('theme.flondr_landing.partials.head')
  
</head>

<body style="background:#2d141e">

    @if(!isset($data['index']))
    
    @endif
    @yield('content')

  



</body>
</html>

