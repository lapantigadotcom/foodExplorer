<div id="gallery" class="screen-shot-gallery">
  <div class="container">
    <div class="section-head text-center">
      <h3><span class="frist"> </span>APPLICATION SCREENSHOTS<span class="second"> </span></h3>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta.</p>
    </div>
  </div>
  <div class="sreen-gallery-cursual">
    <link href="{{ asset('assets/theme/flondr/css/owl.carousel.css')}}" rel="stylesheet">
    <script src="{{ asset('assets/theme/flondr/js/owl.carousel.js')}}"></script>
    <script>
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
        items : 3,
        lazyLoad : true,
        autoPlay : true,
      });
    });
    </script>

    <div class="container">
      <div id="owl-demo" class="owl-carousel">
        <div class="item">
          <img class="lazyOwl" data-src="{{ asset('assets/theme/flondr/images/screen1.jpg')}}" alt="screen-name">
        </div>
        <div class="item">
          <img class="lazyOwl" data-src="{{ asset('assets/theme/flondr/images/screen2.jpg')}}" alt="screen-name">
        </div>
        <div class="item">
          <img class="lazyOwl" data-src="{{ asset('assets/theme/flondr/images/screen3.jpg')}}" alt="screen-name">
        </div>
        <div class="item">
          <img class="lazyOwl" data-src="{{ asset('assets/theme/flondr/images/screen4.jpg')}}" alt="screen-name">
        </div>
        <div class="item">
          <img class="lazyOwl" data-src="{{ asset('assets/theme/flondr/images/screen5.jpg')}}" alt="screen-name">
        </div>
        <div class="item">
          <img class="lazyOwl" data-src="{{ asset('assets/theme/flondr/images/screen6.jpg')}}" alt="screen-name">
        </div>

      </div>
    </div>
  </div>
</div>