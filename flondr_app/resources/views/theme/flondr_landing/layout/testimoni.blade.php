<div class="test-monials text-center">
  <div class="container">
    <span><img src="{{ asset('assets/theme/flondr/images/icon1.png')}}" title="quit"/></span>
    <script>
    $(document).ready(function() {
      $("#owl-demo1").owlCarousel({
        items : 1,
        lazyLoad : true,
        autoPlay : true,
        itemsDesktop : 2,
      });
    });
    </script>
    <div id="owl-demo1" class="owl-carousel">
      <div class="item">
        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
        <div class="quit-people">
          <img src="{{ asset('assets/theme/flondr/images/thumb-people1.jpg')}}" title="name" />
          <h4><a href="#"> John Doe</a></h4>
          <span>Resto Owner</span>
        </div>
      </div>
      <div class="item">
        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
        <div class="quit-people">
          <img src="{{ asset('assets/theme/flondr/images/thumb-people1.jpg')}}" title="name" />
          <h4><a href="#"> Jenna Doe</a></h4>
          <span>Hotel Owner</span>
        </div>
      </div>
      <div class="item">
        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
        <div class="quit-people">
          <img src="{{ asset('assets/theme/flondr/images/thumb-people1.jpg')}}" title="name" />
          <h4><a href="#"> Jessy Doe</a></h4>
          <span>Cafe Owner</span>
        </div>
      </div>
    </div>
  </div>
</div>