<title>  {{ $general->site_title }} </title>
<link href="{{ asset('assets/theme/flondr/css/bootstrap.css')}}" rel='stylesheet' type='text/css' />
<link rel="shortcut icon" href="images/favflondr.png" />  
<script src="{{ asset('assets/theme/flondr/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/theme/flondr/js/move-top.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/theme/flondr/js/easing.js')}}"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
	$(".scroll").click(function(event){   
		event.preventDefault();
		$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
	});
});
</script>
<link href="{{ asset('assets/theme/flondr/css/style.css')}}" rel='stylesheet' type='text/css' />
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> <script>
$(function() {
	var pull    = $('#pull');
	menu    = $('nav ul');
	menuHeight  = menu.height();
	$(pull).on('click', function(e) {
		e.preventDefault();
		menu.slideToggle();
	});
	$(window).resize(function(){
		var w = $(window).width();
		if(w > 320 && menu.is(':hidden')) {
			menu.removeAttr('style');
		}
	});
});
</script>

<script src="{{ asset('assets/theme/flondr/js/responsiveslides.min.js')}}"></script>
<script>
$(function () {
	$("#slider4").responsiveSlides({
		auto: true,
		pager: true,
		nav: true,
		speed: 500,
		namespace: "callbacks",
		before: function () {
			$('.events').append("<li>before event fired.</li>");
		},
		after: function () {
			$('.events').append("<li>after event fired.</li>");
		}
	});

});
</script>

@yield('custom-head')