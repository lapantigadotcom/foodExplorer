 			<div class="footer">
              <div class="container">
                <div class="footer-grids">
                  <div class="col-md-12 footer-grid about-info">
                    <a href="#"><img src="{{asset('assets/theme/flondr/images/logo_flondr.png')}}" title="logo flondr" /></a>
                  </div>

                  <div class="clearfix"> </div>
                  <script type="text/javascript">
                  $(document).ready(function() {
                /*
                var defaults = {
                    containerID: 'toTop', // fading element id
                  containerHoverID: 'toTopHover', // fading element hover id
                  scrollSpeed: 1200,
                  easingType: 'linear' 
                };
                */

                $().UItoTop({ easingType: 'easeOutQuart' });

              });
                  </script>
                  <a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
                </div>
              </div>
            </div>
<link href="{{ asset('assets/theme/flondr/css/popuo-box.css')}}" rel="stylesheet" type="text/css" media="all"/>
      <script src="{{ asset('assets/theme/flondr/js/jquery.magnific-popup.js')}}" type="text/javascript"></script>
 
<!-- <script src="{{ asset('js/extra.js')}}"></script> -->
 
@yield('custom-footer')
