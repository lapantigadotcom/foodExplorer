<style type="text/css">
@media (min-width: 1200px){
	.header__navigation{
		margin-left: 250px;
	}
}
</style>

<header class="header">
	<div class="header__container">
		<div class="container" style="padding-left: 0px; padding-right: 0px">
			<header class="header" style="background-color: #16174F; ">
				<div class="header__logo">
					<a href="">
						<img class="img-responsive" srcset="" alt="logo korlantas" src="{{ asset('images/logo_korlantas.png') }}" style="">
					</a>
					<button data-target="#cargopress-navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
						<span class="navbar-toggle__text">MENU</span>
						<span class="navbar-toggle__icon-bar">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</span>
					</button>
				</div>
				<div class="header__navigation" style="background-color: #16174F; width: 82%">
					<nav class="collapse navbar-collapse" id="cargopress-navbar-collapse">
						<ul class="main-navigation js-main-nav js-dropdown">
							<?php
		                    	$top_menu = $menu->filter(function($v){
			                      	if($v->code=='top_menu'){
			                        	return true;
		                      		}
		                    	});
		                  	?>
			                @foreach($top_menu as $row)
			                    @foreach($row->menuDetail as $val)
			                    <?php 
				                    if($val->parent_id != 0)
				                      continue;
				                    $url = '';
				                    switch ($val->menuType->id) {
				                      case '1':
				                        $url = route('home.category',[ $val->custom ]);
				                        break;
				                      case '2':
				                        $url = route('home.post',[ $val->custom ]);
				                        break;
				                      case '3':
				                        $url = route('home.portfolio',[ $val->custom ]);
				                        break;

				                      case '4':
				                        $url = route('home.rss',[ $val->custom ]);
				                        break;
				                      case '5':
				                        $url = $val->custom;
				                        break; 
				                      case '6':
				                        $url = route('home.products');
				                        break;
				                      case '7':
				                        $url = route('home.mitra');
				                        break;
				                      case '8':
				                        $url = route('home.mitraCity');
				                        break;
				                      case '9':
				                        $url = route('home.type',[ $val->custom ]);
				                        break;
				                      case '10':
				                        $url = route('home.soal_soal');
				                        break;
				                      case '11':
				                        $url = route('home.cctv');
				                        break;
				                      default:
				                        # code...
				                        break;
				                    }
			                    ?>
			                    <li @if($val->submenu()->count() > 0) class="menu-item-has-children" @endif>
			                    	<a style="cursor: pointer; font-size: 12px; font-weight: 150px" 
				                    	@if($val->submenu()->count() > 0 )
				                      		href="#"
				                    	@else
				                      		href="{{ $url }}" 
				                    	@endif
			                    	>
			                    	{{ $val->title }}
			                    	</a>
			                    	@if($val->submenu()->count() > 0 )
			                      		<ul role="menu" class="sub-menu">
			                      		@foreach($val->submenu as $v)
			                        		<?php 
			                          			$url = '';
				                          		switch ($v->menuType->id) {
						                            case '1':
						                              $url = route('home.category',[ $v->custom ]);
						                              break;
						                            case '2':
						                              $url = route('home.post',[ $v->custom ]);
						                              break;
						                            case '3':
						                              $url = route('home.portfolio',[ $v->custom ]);
						                              break;
						                            case '4':
						                              $url = route('home.rss',[ $v->custom ]);
						                              break;
						                            case '5':
						                                $url = $v->custom;
						                                break; 
						                            case '6':
						                              $url = route('home.products');
						                              break;
						                            case '7':
						                              $url = route('home.mitra');
						                              break;
						                            case '8':
						                              $url = route('home.mitraCity');
						                              break;
						                            case '9':
						                              $url = route('home.type',[ $v->custom ]);
						                              break;
						                            case '10':
						                              $url = route('home.soal_soal');
						                              break;
						                            case '11':
						                              $url = route('home.cctv');
						                              break;
						                            default:
						                              # code...
						                              break;
						                        }
					                        ?>
			                        		<li style="background-color: white; width:120%">
			                          			<a href="{!! url($url) !!}">{!! $v->title !!}</a>
			                          			@if($v->submenu()->count() > 0 )
			                          			<ul class="dropdown" style="background-color: white">
													@foreach($v->submenu as $w)
													<?php 
														$url = '';
														switch ($w->menuType->id) {
															case '1':
																$url = route('home.category',[ $w->custom ]);
																break;
													    	case '2':
														    	$url = route('home.post',[ $w->custom ]);
														    	break;
														    case '3':
														      	$url = route('home.portfolio',[ $w->custom ]);
														      	break;
														    case '4':
														      	$url = route('home.rss',[ $w->custom ]);
														      	break;
														    case '5':
														        $url = $w->custom;
														        break; 
														    case '6':
														      	$url = route('home.products');
														      	break;
														    case '7':
														      	$url = route('home.mitra');
														      	break;
														    case '8':
														      	$url = route('home.mitraCity');
														      	break;
														    case '9':
														      	$url = route('home.type',[ $w->custom ]);
														      	break;
														    case '10':
														      	$url = route('home.soal_soal');
														      	break;
														    case '11':
														      	$url = route('home.cctv');
														      	break;
														    default:
														      # code...
														      	break;
													  	}
													?>
													<li class="">
														<a href="{!! url($url) !!}" >{!! $w->title !!}</a>
													</li>
													@endforeach
					                            </ul>
					                            @endif
			                        		</li>
			                        	@endforeach
			                      		</ul>
			                    	@endif
			                    </li>
			                    @endforeach
							@endforeach
						</ul>
					</nav>
				</div>
				<div class="header__widgets" style="background: url('{{asset('images/bg_header-korlantas.jpg')}}')">
					<style type="text/css">
					.icon-box{
						padding-top: 0px
					}
					</style>
					<div class="widget-icon-box">
						<div class="icon-box">	
							<h4 class="icon-box__title">Call Center</h4>
							<span class="icon-box__subtitle">{{$data['contact']->telephone}}</span>
						</div>
					</div>
					<div class="widget-icon-box">
						<div class="icon-box">
							<h4 class="icon-box__title">SMS Center</h4>
							<span class="icon-box__subtitle">{{$data['contact']->telephone}}</span>
						</div>
					</div>
				</div>
				<div class="header__navigation-widgets" style="background-color: #16174F">
				</div>
			</header>
		</div>
	</div>
</header>