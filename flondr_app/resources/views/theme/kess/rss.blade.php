@extends('theme.hasanahmulia.layout.index')

@section('content')
   <div class="" >
  <div class="row">
    <h1 class="title-section bold"><span class="nbr">Our </span>Blog<span class="border"></span> </h1>
    <h2 class="subtitle-section">{{ isset($data['context'])?$data['context']:'' }}</h2>
    </div>
</div>

<div class="row">    
    <hr>
    @foreach($data['content'] as $v)
    <div class="large-12 columns">
        <div class="large-1 columns date-news">
            <div class="month"> {{ date('M', strtotime($v->get_date())) }}</div>
            <div class="date">{{ date('d', strtotime($v->get_date())) }}</div>
        </div>
        <div class="large-11 columns right-news">
             <div class="large-12 columns title-news">
                 <h3>{{ $v->get_title() }}</h3>
                 <div class="desc-news">Posted on {{ date('M', strtotime($v->get_date())) }} {{ date('d', strtotime($v->get_date())) }}</div>
             </div>
             <div class="large-12 columns content-news">
                 <p>{!! substr($v->get_description(),0,250) !!} &nbsp;
                 <span><a href="{!! URL::to($v->get_permalink()) !!}" target="_blank">[ Read More]</a></span></p>
             </div>
        </div>
    </div>
    <hr>
    @endforeach
    
</div>
@endsection