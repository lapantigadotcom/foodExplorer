@extends('theme.kess.layout.index')

@section('content')                
<!-- Main Container Starts -->
    
    <div style='margin: 25 25px 25px 0'>

     <div class="main-container">
    <!-- Nested Row Starts -->
    <div class="row">
      <div class="col-md-9 col-xs-12">
        <div id="calendar"></div>
      </div>
    </div>
      </div>  </div>
  
<!-- Main Container Ends -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/tema/kessv1/css/kalender.css') }}">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">  
var eventJson ='<?php echo json_encode($data['event']) ?>';
var months = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
"Juli", "Agustus", "September", "Oktober", "November", "Desember"];
var weekdays = ["Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu",
"Minggu"];


function getEvents()
{
  eventJson = eventJson.replace(/#/g, " ");
  eventJson = eventJson.replace(/\r\n/g, "<br>");
  eventJson = eventJson.replace(/\r/g, "<br>");
  eventJson = eventJson.replace(/\n/g, "<br>");
  eventJson = eventJson.replace(/\\/g, "");
  var result = jQuery.parseJSON(eventJson);
  var counter = 0;
  for(var k in result) {
      if(result[k] == null)
        continue;
      //alert(result[k].name);
      console.log("event id=" + k, result[k]);
      result[k].title = result[k].name;
      result[k].start = new Date(result[k].date);
      if(counter < 5)
        getNearestEvent(result[k], k);
  }
  return result;
}

function getNearestEvent(data, k)
{
  //alert(data.date);
  var dateNow = new Date();
  var dateEvent = new Date(data.date);
  console.log("date compare", dateNow + " <> " + dateEvent);
  if(dateEvent >= dateNow)
    $("#accordion-faqs").append(getAppendedItem(data, k));
}

function getAppendedItem(data, k) 
{
  //alert("append " + data);
  var dateEvent = new Date(data.date);
  var tmpDate = data.date.split("-");
  var appendStr = "";
  var day = dateEvent.getDay();
  var month = dateEvent.getMonth();
  var year = dateEvent.getFullYear();
  var strDate = weekdays[day] + ", " + tmpDate[2] + " " + months[month] + " " + year;

  //<!-- Panel Heading Starts -->
  var str1 = "<div class='panel'>";
  str1 += "<div class='panel-heading'>";
  str1 += "<h5 class='panel-title'>";
  str1 += "<a class='collapsed' aria-expanded='false' data-toggle='collapse' data-parent='#accordion-faqs' href='#collapse" + k + "'>";
  str1 += strDate + " - " + "<strong>" + data.name + "</strong>"; 
  str1 += "<i class='fa pull-left fa-calendar'></i>  "
  str1 += "<span class='fa pull-right fa-plus'></span></a>";
  str1 += "</h5>";
  str1 += "</div>";
  //<!-- Panel Heading Ends -->
  //<!-- Panel Body Starts -->
  str1 += "<div style='height: 0px;' aria-expanded='false' id='collapse" + k + "' class='panel-collapse collapse'>";
  str1 += "<div class='panel-body'>";
  str1 += "<p>";
  str1 += "<ul class='list-unstyled'>";
  str1 += "<li class='row'>";
  str1 += "<span class='col-sm-2 col-xs-12'>";
  str1 += "<strong>Tanggal</strong><br>";
  str1 += "<strong>Deskripsi</strong><br>";
  str1 += "</span>";
  str1 += "<span class='col-sm-10 col-xs-12'>";
  str1 += data.date + "<br>";
  str1 += data.description + "<br>";
  str1 += "</span>";
  str1 += "</li>";
  str1 += "</ul>";
  str1 += "</p>";
  str1 += "</div>";
  str1 += "</div>";
  //<!-- Panel Body Ends -->
  appendStr = str1;

  return appendStr;
}

$.getScript('http://arshaw.com/js/fullcalendar-1.6.4/fullcalendar/fullcalendar.min.js',function(){

  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();
  var events = getEvents();

  $('#calendar').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month'
    },
    editable: true,
    events: events
  });
})
</script>
@endsection