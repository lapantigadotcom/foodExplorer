@extends('theme.kess.layout.index')

@section('content')                       
<section id="home">
  <div class="container-fluid p-0">

   @include('theme.kess.layout.slider')

  </div>
</section>

  @include('theme.kess.layout.news')

  @include('theme.kess.layout.search')

  @include('theme.kess.layout.front_about')
  @include('theme.kess.layout.statistik')
  @include('theme.kess.layout.soal')
  @include('theme.kess.layout.sambutan')
  @include('theme.kess.layout.testimoni')
 
 
 @endsection