<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>

<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="description" content="kess" />
<meta name="keywords" content="kess" />
<meta name="author" content="lapantiga.com" />

<!-- Page Title -->
<title>
  {{ $general->site_title }} 
</title>

@include('theme.kess.partials.head')
</head>
<body class="boxed-layout pt-40 pb-40 pt-sm-0" data-bg-img="{{ asset('images/pattern/dot-lapantiga.png')}}">
    <div id="wrapper" class="clearfix">
     
    @include('theme.kess.partials.menu')
      <!-- Start main-content -->
      <div class="main-content shadow">
        @if(!isset($data['index']))
          
           
        
        @endif
        @yield('content')
        
      <!-- end main-content -->
      </div>


      @include('theme.kess.partials.footer')
      @include('theme.kess.partials.script')
     
</body>
</html>