  <div class="rev_slider_wrapper">
      <div class="rev_slider rev_slider_default" data-version="5.0">
        <ul>
          <?php
          $rs = 0;
          ?>
          @foreach($data['slider'] as $slider)
          <!-- SLIDE 2 -->
          <li data-index="rs-{{++$rs}}" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="{{asset('upload/media/'.$slider->file)}}" data-rotate="0" data-saveperformance="off" data-title="{{$slider->title}}" data-description="">
            <!-- MAIN IMAGE -->
            <img src="{{asset('upload/media/'.$slider->file)}}"  alt=""  data-bgposition="center 40%" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10" data-no-retina>
            <!-- LAYERS -->

            <!-- LAYER NR. 1 -->
            <div class="tp-caption tp-resizeme text-uppercase  bg-dark-transparent-5 text-white font-raleway border-left-theme-color-2-6px border-right-theme-color-2-6px pl-30 pr-30"
              id="rs-2-layer-1"
              data-x="['center']"
              data-hoffset="['0']"
              data-y="['middle']"
              data-voffset="['-90']" 
              data-fontsize="['28']"
              data-lineheight="['54']"
              data-width="none"
              data-height="none"
              data-whitespace="nowrap"
              data-transform_idle="o:1;s:500"
              data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
              data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
              data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
              data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
              data-start="1000" 
              data-splitin="none" 
              data-splitout="none" 
              data-responsive_offset="on"
              style="z-index: 7; white-space: nowrap; font-weight:400; border-radius: 30px;">{{$slider->h1}}
            </div>

            <!-- LAYER NR. 2 -->
            <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway pl-30 pr-30"
              id="rs-2-layer-2"
              data-x="['center']"
              data-hoffset="['0']"
              data-y="['middle']"
              data-voffset="['-20']"
              data-fontsize="['48']"
              data-lineheight="['70']"
              data-width="none"
              data-height="none"
              data-whitespace="nowrap"
              data-transform_idle="o:1;s:500"
              data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
              data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
              data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
              data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
              data-start="1000" 
              data-splitin="none" 
              data-splitout="none" 
              data-responsive_offset="on"
              style="z-index: 7; white-space: nowrap; font-weight:700; border-radius: 30px; background: #baa600">{{$slider->h2}}
            </div>

            <!-- LAYER NR. 3 -->
            <div class="tp-caption tp-resizeme text-white text-center" 
              id="rs-2-layer-3"
              data-x="['center']"
              data-hoffset="['0']"
              data-y="['middle']"
              data-voffset="['50']"
              data-fontsize="['16']"
              data-lineheight="['28']"
              data-width="none"
              data-height="none"
              data-whitespace="nowrap"
              data-transform_idle="o:1;s:500"
              data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
              data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
              data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
              data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
              data-start="1400" 
              data-splitin="none" 
              data-splitout="none" 
              data-responsive_offset="on"
              style="z-index: 5; white-space: nowrap; letter-spacing:0px; font-weight:400;">
              {{$slider->h3}}
              
            </div>
            <!-- LAYER NR. 4 -->  
          </li>
          @endforeach
        </ul>
      </div>
    <!-- end .rev_slider -->
    </div>