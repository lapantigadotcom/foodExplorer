<div class="featured" style="background:#eee">
  <div class="section-head text-center">
    <h3><span class="frist"> </span>Find FLONDR On<span class="second"> </span></h3>
  </div>
  <script>
  $(document).ready(function() {
    $("#owl-demo2").owlCarousel({
      items : 3,
      lazyLoad : true,
      autoPlay : true,
      pagination : false,
    });
  });
  </script>
  <div id="owl-demo2" class="owl-carousel" style="width:50%">
    <div class="item">
      <img src="{{ asset('assets/theme/flondr/images/fb-logo.jpg')}}" title="fb" />
    </div>
    <div class="item">
      <img src="{{ asset('assets/theme/flondr/images/twitter-logo.jpg')}}" title="twitter" />
    </div>
    <div class="item">
      <img src="{{ asset('assets/theme/flondr/images/instagram-logo.jpg')}}" title="instagram" />
    </div>

  </div>
</div>