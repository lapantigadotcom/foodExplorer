<section id="event">
  <div class="container">
    <div class="section-content">
     <div class="row">
        <div class="col-md-5"> 
         <img src="{{ asset('assets/tema/kessv1/images/logo_kess_500px.jpg')}}" class="img-fullwidth" alt="kess">
        </div>
        <div class="col-md-7 pb-sm-20">
          <!-- <h3 class="title line-bottom mb-20 font-28 mt-0 line-height-1">Kata <span class="text-theme-color-2 font-weight-400">Sambutan</span></h3> -->
          <h3 class="title line-bottom mb-20 font-28 mt-0 line-height-1">{{$data['sambutan']->title}}</span></h3>
          <!-- <h6 class="text-uppercase letter-space-5  title font-playfair text-uppercase">Kepala Korps Lalu Lintas Polri<br>Drs. Agung Budi Maryoto, MSi.</h6> -->
           <p class="mb-20">
            @if(strlen($data['sambutan']->description) > 320)
              {!!substr($data['sambutan']->description, 0, 319)!!}...
              <b>Selengkapnya></b>
            @else
              {!!$data['sambutan']->description!!}
            @endif
          </p>
          <div class="col-sm-6 col-md-3 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
            <div class="icon-box text-center pl-0 pr-0 mb-0">
              <a href="#" class="icon bg-theme-colored icon-circled icon-border-effect effect-circle icon-md">
                <i class="fa fa-facebook text-white"></i>
              </a>
              <h5 class="icon-box-title mt-15 mb-10 letter-space-4 text-uppercase"><strong>Facebook</strong></h5>
            </div>
          </div>
          <div class="col-sm-6 col-md-3 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
            <div class="icon-box text-center pl-0 pr-0 mb-0">
              <a href="#" class="icon bg-theme-color-2 icon-circled icon-border-effect effect-circle icon-md">
                <i class="fa fa-twitter text-white"></i>
              </a>
              <h5 class="icon-box-title mt-15 mb-10 letter-space-4 text-uppercase"><strong>Twitter</strong></h5>
            </div>
          </div>
          <div class="col-sm-6 col-md-3 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
            <div class="icon-box text-center pl-0 pr-0 mb-0">
              <a href="#" class="icon bg-theme-colored icon-circled icon-border-effect effect-circle icon-md">
                <i class="fa fa-instagram text-white"></i>
              </a>
              <h5 class="icon-box-title mt-15 mb-0 letter-space-4 text-uppercase"><strong>Instagram</strong></h5>
            </div>
          </div>
         </div>
       </div>
     </div>
   </div>
</section>