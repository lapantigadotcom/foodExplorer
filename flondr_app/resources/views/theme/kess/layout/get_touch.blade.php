<div id="contact" class="getintouch">
  <div class="container">
    <div class="section-head text-center">
      <h3><span class="frist"> </span>GET IN TOUCH WITH FLONDR<span class="second"> </span></h3>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta.</p>
    </div>

    <div class="col-md-9 getintouch-left">
      <div class="contact-form col-md-10">
        <h3>Say hello!</h3>
        <form>

          <input type="text" placeholder="Name" id="name" required data-validation-required-message="Please enter your name." />
          <input type="text"  placeholder="Email" required data-validation-required-message="Please enter your Email."  />
          <input type="text"  placeholder="phone"/>
          <textarea placeholder="Message" required data-validation-required-message="Please enter your message."  /> </textarea>
          <input type="submit" />
        </form>
      </div>
      <ul class="footer-social-icons col-md-2 text-center">

        <div class="clearfix"> </div>
      </ul>
    </div>
    <div class="col-md-2 getintouch-left">
      <div class="footer-divice">
        <img src="{{ asset('assets/theme/flondr/images/mock-bottom.png')}}" title="getintouch" />
      </div>
    </div>  
  </div>
</div>

</div>