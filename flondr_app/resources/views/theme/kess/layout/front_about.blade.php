<!-- Section:about-->
<section>
  <div class="container pb-60">
    <div class="section-content">
      <div class="row">
        <div class="col-md-8">
          <div class="meet-doctors">
            <h2 class="text-uppercase mt-0 line-height-1"><span class="text-theme-colored">Selamat datang  </span></h2>
            <h6 class="text-uppercase letter-space-5 line-bottom title font-playfair text-uppercase">di official website Komunitas Ekonomi Syariah Surabaya</h6>
            <p>Sebuah komunitas ekonomi syariah untuk kemajuan tatanan dan tuntunan  demi membangun<b> Negara Kesatuan Republik Indonesia</b>. Konsep dan program Bisnis:</p>
          </div>
          <div class="row mb-sm-30">
            <div class="col-sm-6 col-md-6">
              <div class="icon-box p-0 mb-20">
               <a class="icon bg-theme-colored icon-circled icon-border-effect effect-circle icon-sm pull-left sm-pull-none flip">
                <i class="fa fa-cart-plus text-white"></i>
               </a>
               <div class="ml-70 ml-sm-0">
                <h5 class="icon-box-title mt-10 text-uppercase letter-space-2 mb-10">Perdagangan</h5>
                <p class="text-gray" style="font-size:12px">Membangkitkan ekonomi di bidang perdagangan </p>
               </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-6">
              <div class="icon-box p-0 mb-20">
               <a class="icon bg-theme-color-2 icon-circled icon-border-effect effect-circle icon-sm pull-left sm-pull-none flip">
                <i class="fa fa-leaf text-white"></i>
               </a>
               <div class="ml-70 ml-sm-0">
                <h5 class="icon-box-title mt-10 text-uppercase letter-space-2 mb-10">Pertanian</h5>
                <p class="text-gray" style="font-size:12px">Membangkitkan ekonomi di bidang Pertanian </p>
               </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-6">
              <div class="icon-box p-0 mb-20">
               <a class="icon bg-theme-color-2 icon-circled icon-border-effect effect-circle icon-sm pull-left sm-pull-none flip">
                <i class="fa fa-paw text-white"></i>
               </a>
               <div class="ml-70 ml-sm-0">
                <h5 class="icon-box-title mt-10 text-uppercase letter-space-2 mb-10">Peternakan</h5>
                <p class="text-gray" style="font-size:12px">Membangkitkan ekonomi di bidang Peternakan </p>
               </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-6">
              <div class="icon-box p-0 mb-20">
               <a class="icon bg-theme-colored icon-circled icon-border-effect effect-circle icon-sm pull-left sm-pull-none flip">
                <i class="fa fa-external-link text-white"></i>
               </a>
               <div class="ml-70 ml-sm-0">
                <h5 class="icon-box-title mt-10 text-uppercase letter-space-2 mb-10">Perkerbunan</h5>
                <p class="text-gray" style="font-size:12px">Membangkitkan ekonomi di bidang Perkerbunan </p>
               </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-6">
              <div class="icon-box p-0">
               <a class="icon bg-theme-colored icon-circled icon-border-effect effect-circle icon-sm pull-left sm-pull-none flip">
                <i class="fa fa-bank text-white"></i>
               </a>
               <div class="ml-70 ml-sm-0">
                <h5 class="icon-box-title mt-10 text-uppercase letter-space-2 mb-10">Jasa Keuangan</h5>
                <p class="text-gray" style="font-size:12px">Membangkitkan ekonomi di bidang Jasa Keuangan </p>
               </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-6">
              <div class="icon-box p-0">
               <a class="icon bg-theme-color-2 icon-circled icon-border-effect effect-circle icon-sm pull-left sm-pull-none flip">
                <i class="fa fa-building text-white"></i>
               </a>
               <div class="ml-70 ml-sm-0">
                <h5 class="icon-box-title mt-10 text-uppercase letter-space-2 mb-10">Properti</h5>
                <p class="text-gray" style="font-size:12px">Membangkitkan ekonomi di bidang Properti </p>
               </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-6">
              <div class="icon-box p-0">
               <a class="icon bg-theme-color-2 icon-circled icon-border-effect effect-circle icon-sm pull-left sm-pull-none flip">
                <i class="fa fa-mortar-board text-white"></i>
               </a>
               <div class="ml-70 ml-sm-0">
                <h5 class="icon-box-title mt-10 text-uppercase letter-space-2 mb-10">Pendidikan</h5>
                <p class="text-gray" style="font-size:12px">Membangkitkan ekonomi di bidang Pendidikan </p>
               </div>
              </div>
            </div>
             

          </div>
        </div>
        <div class="col-md-4">
         <div class="p-30 mt-0 bg-theme-colored">
          <h3 class="title-pattern mt-0"><span class="text-white">Kotak <span class="text-theme-color-9">Surat</span></span></h3>
          <!-- Appilication Form Start-->
          {!! Form::open(array('route' => 'home.adukan', 'method' => 'post', 'class'=>'reservation-form mt-20', 'id' =>'reservation_form')) !!}
          <!-- <form id="reservation_form" name="reservation_form" class="reservation-form mt-20" method="post" action="{{URL::to('adukan')}}"> -->
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group mb-20">
                  <input placeholder="Nama Lengkap" type="text" id="reservation_name" name="nama_lengkap" required="" class="form-control">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group mb-20">
                  <input placeholder="Email" type="text" id="reservation_email" name="email" class="form-control" required="">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group mb-20">
                  <input placeholder="Handphone" type="text" id="reservation_phone" name="hp" class="form-control" required="">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group mb-20">
                  <div class="styled-select">
                    <?php
                    $kategoriList = [];
                    foreach ($data['kategori_aduan'] as $key => $value) {
                      $kategoriList[$value->id] = $value->title;
                    }
                    ?>
                    {!! Form::select('kategori', $kategoriList, null, ['class' => 'form-control', 'id' => 'kategori', 'required']) !!}
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group mb-20">
                  <input placeholder="Alamat Lengkap" type="text" id="alamat" name="alamat" class="form-control" required="">
                </div>
              </div>
              <div class="col-sm-12 ">
                <div class="form-group">
                  <textarea placeholder="Pesan.." rows="3" class="form-control required" name="isi" id="form_message" aria-required="true"></textarea>
                </div>
              </div>
              <div class="col-sm-12 text-center">
                <div class="form-group mb-20">
                  <img src="./images/captcha.jpg">
                </div>
              </div>

              <div class="col-sm-12">
                <div class="form-group mb-0 mt-10">
                  <input name="form_botcheck" class="form-control" type="hidden" value="">
                  <button type="submit" class="btn btn-colored btn-theme-color-2 text-white btn-lg btn-block" data-loading-text="Please wait...">Kirim ></button>
                </div>
              </div>
            </div>
          <!-- </form> -->
          {!! Form::close() !!}
          <!-- Application Form End-->           
          </div>
        </div>
      </div>
    </div>
  </div>
</section>