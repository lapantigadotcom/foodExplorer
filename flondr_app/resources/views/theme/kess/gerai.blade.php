@extends('theme.kess.layout.index')
@section('content')
<style type="text/css">
.news-post{
  background-color: #fefefe;
  -webkit-transition: background-color 0.5s;
  border-top: solid 0.5px #ccc; 
  border-bottom: solid 0.5px #ccc; 
  margin: 10px; 
  padding: 10px
}
.news-post:hover{
  background-color: #efefef;
}

.post-title h4{
  color: #000;
  -webkit-transition: color 0.5s;
}

.post-title h4:hover{
  color: #16174F;
}

</style>
<div style='margin: 0 25px 25px 25px'>
  
  <!-- Main Banner Ends -->
  <!-- Main Container Starts -->
  <div class="main-container">
    <!-- Nested Row Starts -->
    <div class="row">
      <div class="col-md-12 col-xs-12">
        <h2>Lokasi Koperasi KESS</h2>
        <h6 class="main-heading">Pastikan <span style="color:red">"Share location browser Anda aktif"</span> | Radius lokasi adalah 30KM</h6>
        <!-- MAP START -->
        <div id="map"></div>
        <h5 class="main-heading">Note:</h5>
        <h6 class="main-heading"><img src="{{ asset('img/marker.png') }}"> = Lokasi Koperasi
          <img src="{{ asset('img/marker_now.png') }}"> = Lokasi Anda
        </h6>
        <style>
          #map {
            width: 100%;
            height: 400px;
            background-color: #CCC;
          }
        </style>
        <script type="text/javascript">
          var mitraJson ='<?php echo json_encode($data['mitraDetail']) ?>';
          var currentLocation = '';
          var nearbyDistance = 100;
          var map = '';
          var custom_marker = '';
          var custom_marker_now = '';

          //google maps
          function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
              center: {lat: -34.397, lng: 150.644},
              zoom: 16
            });
            custom_marker =  { 
              url: "{{ asset('img/marker.png') }}"
            };
            custom_marker_now =  { 
              url: "{{ asset('img/marker_now.png') }}"
            };
            //var infoWindow = new google.maps.InfoWindow({map: map});
            // Try HTML5 geolocation.
            if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                  lat: position.coords.latitude,
                  lng: position.coords.longitude
                };

                //currentLocation = position.coords.latitude + "," + position.coords.longitude;
                currentLocation = pos;
                //alert(currentLocation);
                //currentLocation = "<?php $userCoord = '" + currentLocation + "';?>";
                
                //infoWindow.setPosition(pos);
                //infoWindow.setContent('Your Location');
                var marker = new google.maps.Marker({
                  position: pos,
                  map: map,
                  title: "Your location",
                  icon: custom_marker_now
                });
                map.setCenter(pos);

                showNearbyMitra();
              }, function() {
                handleLocationError(true, infoWindow, map.getCenter());
              });
            } else {
              // Browser doesn't support Geolocation
              handleLocationError(false, infoWindow, map.getCenter());
            }
            showAllMitraOnMaps();
          }

          function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                                  'Error: The Geolocation service failed.' :
                                  'Error: Your browser doesn\'t support geolocation.');
          }

          function showNearbyMitra() {
            //alert(mitraJson);
            var result = jQuery.parseJSON(mitraJson);
            for(var k in result) {
              if(k == 0)
                continue;
              console.log("mitra id=" + k, result[k]);
              //calculate distance
              var tmpCoord = result[k].koordinat.split(",");
              var tmpDistance = calculateDistance(
                currentLocation.lat, currentLocation.lng, tmpCoord[0], tmpCoord[1]);
              console.log("distance", tmpDistance);
              //if distance <= nearbyDistance, append
              if(tmpDistance <= nearbyDistance){
                console.log("show", "show mitra");
                $("#accordion-faqs").append(getAppendedMitra(result[k], k));
              }
              //else skip

            }
          }  

          function showAllMitraOnMaps() {
            var result = jQuery.parseJSON(mitraJson);
            //alert(mitraJson);
            for(var k in result) {
              if(k == 0)
                continue;
              //create marker
              var tmpCoord = result[k].koordinat.split(",");
              var myLatLng = { lat: parseFloat(tmpCoord[0]), lng: parseFloat(tmpCoord[1])};
              console.log("coord", myLatLng);
              var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: result[k].nama,
                icon: custom_marker
              });
            }
          }

          function getAppendedMitra(mitra, k) {
            var appendStr = "";

            //<!-- Panel Heading Starts -->
            var str1 = "<div class='panel'>";
            str1 += "<div class='panel-heading'>";
            str1 += "<h5 class='panel-title'>";
            str1 += "<a class='collapsed' aria-expanded='false' data-toggle='collapse' data-parent='#accordion-faqs' href='#collapse" + k + "'>";
            str1 += mitra.title;
            str1 += "<span class='fa pull-right fa-plus'></span></a>";
            str1 += "</h5>";
            str1 += "</div>";
            //<!-- Panel Heading Ends -->
            //<!-- Panel Body Starts -->
            str1 += "<div style='height: 0px;' aria-expanded='false' id='collapse" + k + "' class='panel-collapse collapse'>";
            str1 += "<div class='panel-body'>";
            str1 += "<p>";
            str1 += "<div class='panel-body'>";
            str1 += "<ul class='list-unstyled'>";
            str1 += "<li class='row'>";
            str1 += "<span class='col-sm-4 col-xs-12'><strong>Deskripsi</strong><br>";
            str1 += "<strong>Alamat</strong><br>";
            str1 += "</span>";
            str1 += "<span class='col-sm-8 col-xs-12'>";
            str1 += mitra.description + "<br>";
            str1 += mitra.address + "<br>";
            str1 += "</span>";
            str1 += "</li>";
            str1 += "</ul>";
            str1 += "</div>";
            str1 += "</p>";
            str1 += "</div>";
            str1 += "</div>";
            //<!-- Panel Body Ends -->
            appendStr = str1;

            return appendStr;
          }

          function calculateDistance(lat1, lon1, lat2, lon2) {
            var R = 6371; // Radius of the earth in km
            var dLat = deg2rad(lat2-lat1);  // deg2rad below
            var dLon = deg2rad(lon2-lon1); 
            var a = 
              Math.sin(dLat/2) * Math.sin(dLat/2) +
              Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
              Math.sin(dLon/2) * Math.sin(dLon/2)
              ; 
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
            var d = R * c; // Distance in km
            return d;
          }

          function deg2rad(deg) {
            return deg * (Math.PI/180)
          }  
        </script>
        <script async defer
            src="https://maps.googleapis.com/maps/api/js?key= AIzaSyBYUI_Rkz8TYKFxOtci-WIwLwIsElHBeQ8&callback=initMap">
        </script> 
           <div class="spacer-block"></div>
        <h3 class="main-heading2 nomargin-top">Gerai SIM Terdekat</h3>
        <div class="panel-group" id="accordion-faqs">
        </div>
      </div>
       
  <style type="text/css">
  .img.dsq-widget-avatar{height: 50px}
  .dsq-widget-meta{font-size: 12px; color: red}
  .dsq-widget-comment{font-size: 12px}
  .dsq-widget-list{padding: 0px;margin: 0px}
  .dsq-widget-meta a{font-size: 12px;color: red}
  </style>
 

     </div>     
  </div>
  <!-- section FB -->
</div>         
@include('theme.kess.layout.search')               
@endsection

      
