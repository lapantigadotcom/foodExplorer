@extends('theme.korlantas.layout.index')
@section('content')
<title>Korlantas Polri | {{ $data['content']->title }}</title>
<style type="text/css">
.cctv-description{
  border: solid 1px #efefef;
  padding: 15px;
}
</style>
<section id="blog" class="">
  <!-- Main Container Starts -->
  <div class="container pt-60 pb-60">

    <div class="main-container">
      <!-- Nested Row Starts -->
      <div class="row">
        <!-- Mainarea Starts -->
        <div class="col-md-9 col-xs-12">
          <!-- News Post Single Starts -->
          <div class="news-post-single">
            <!-- News Post Starts -->
            <article class="news-post">
              <div class="inner">
                <h2>{{ $data['content']->title }}</h2>
                <div class="news-post-content">
                  <div class="cctv-description">
                    {!! $data['content']->description !!}
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      {!! $data['content']->embed!!}
                    </div>
                  </div>
                   <div class="addthis_native_toolbox">
                  </div>
                </div>
              </div>
            </article>
          <!-- News Post Ends -->
          </div>
        </div>
        @include('theme.korlantas.layout.sidebar_artikel')
      </div>
    </div>
  </div>
</section>@include('theme.korlantas.layout.search')
 <style type="text/css">
                ul.post-meta {
                  border-bottom: 0.5px solid #ccc; 
                  margin-bottom: 12px;
                }
                ul.post-meta li{
                  padding: 6px
                }
                .news-post-content{
                  margin-top: 1em;
                  margin-bottom: 1em;
                  padding: 1em;
                  padding-left: 0px;
                   border-bottom: 0.5px solid #ccc;
                }
                </style>
@endsection
@section('custom-footer')
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56a630c285f8f2c7" async="async"></script>
 
@endsection