@extends('theme.korlantas.layout.index')

@section('content')

<div class="main-content">
 <!-- Divider: Contact -->
    <section class="divider">
      <div class="container pt-0">
        <div class="row mb-60 bg-deep">
          <div class="col-sm-12 col-md-4">
            <div class="contact-info text-center pt-60 pb-60 border-right">
              <i class="fa fa-phone font-36 mb-10 text-theme-colored"></i>
              <h4>Call Center</h4>
              <h6 class="text-gray">Phone: {{$data['contact']->telephone}}</h6>
            </div>
          </div>
          <div class="col-sm-12 col-md-4">
            <div class="contact-info text-center  pt-60 pb-60 border-right">
              <i class="fa fa-map-marker font-36 mb-10 text-theme-colored"></i>
              <h4>Address</h4>
              <h6 class="text-gray">{{$data['contact']->address}}</h6>
            </div>
          </div>
          <div class="col-sm-12 col-md-4">
            <div class="contact-info text-center  pt-60 pb-60">
              <i class="fa fa-envelope font-36 mb-10 text-theme-colored"></i>
              <h4>Email</h4>
              <h6 class="text-gray">{{$data['contact']->email}}</h6>
            </div>
          </div>
        </div>
        <div class="row pt-10">
          <div class="col-md-5">
          <h4 class="mt-0 mb-30 line-bottom">Korlantas Polri</h4>
          <!-- Google Map HTML Codes -->
<div 
  data-address="121 King Street, Melbourne Victoria 3000 Australia"
  data-popupstring-id="#popupstring1"
  class="map-canvas autoload-map"
  data-mapstyle="default"
  data-height="400"
  data-latlng="22.798835,89.534401"
  data-title="sample title"
  data-zoom="12"
  data-marker="images/map-icon-blue.png">
</div>
<div class="map-popupstring hidden" id="popupstring1">
  <div class="text-center">
    <h3>Studypress Office</h3>
    <p>121 King Street, Melbourne Victoria 3000 Australia</p>
  </div>
</div>
<!-- Google Map Javascript Codes -->
<script src="http://maps.google.com/maps/api/js"></script>
<script src="js/google-map-init.js"></script>
          <div class="map-popupstring hidden" id="popupstring1">
            <div class="text-center">
              <h3>Korlantas Polri</h3>
              <p>{{$data['contact']->address}}</p>
            </div>
          </div>
          </div>
          <div class="col-md-7">
            <h4 class="mt-0 mb-30 line-bottom">Silahkan isi form dibawah</h4>
            <!-- Contact Form -->
            @include('page.partials.notification')
            @if (count($errors) > 0)
              <div class="alert alert-danger">
                <strong>Whoops!</strong> Form yang anda isi belum lengkap.<br><br>
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              @endif
              {!! Form::open(['route'=>'home.doContact', 'method' => 'POST','files' => true]) !!}              
                @include('theme.korlantas.contactus.form',['buttonSubmit' => 'SUBMIT'])
              {!! Form::close() !!}
              </div>
              
            
          </div>
        </div>
      </div>
    </section>
 
  <!-- end main-content -->

 
             @include('theme.korlantas.layout.search')

</div>
@endsection
  
@section('custom-head')
 

@endsection

@section('custom-footer') 
@endsection