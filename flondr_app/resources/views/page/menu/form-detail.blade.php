					<div class="form-group">
						{!! Form::label('title','Nama : ') !!}
						{!! Form::text('title',null, ['class' => 'form-control']) !!}
						{!! Form::hidden('ms_menu_id',$menuId, ['class' => 'form-control']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('code','Kode : ') !!}
						{!! Form::text('code',null, ['class' => 'form-control']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('parent_id','Parent : ') !!}
						{!! Form::select('parent_id',$arrMenuDetail,null, ['class' => 'form-control']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('ms_menu_type_id','Tipe : ') !!}
						{!! Form::select('ms_menu_type_id',$arrMenuType,null, ['class' => 'form-control','id' => 'ms_menu_type_id']) !!}
					</div>
					<div class="form group">
						<div id="menu_type_container" class="row">
							@if(isset($data['detail']))
							@if($data['content']->ms_menu_type_id !='5')
							@foreach($data['detail'] as $row)
							<div class='col-md-6'>
								<label>

								{!! Form::radio('custom', $row->id, null) !!} {{ substr($row->title,0,15) }}...
								
									
								</label>
							</div>
							@endforeach
							@else
								<div class="col-md-12">
									<label for='custom'>Link : </label>
									<input id='custom' type='text' class='form-control' name='custom' placeholder='ketikan link. (Misal: http://innvaderstudio.com)' value="{{ $data['content']->custom }}" /><br>
								</div>
							@endif
							@endif
						</div>
						<div id="category-container">
							
						</div>
					</div>
					<div class="form-group">
						{!! Form::submit($buttonSubmit,['class' => 'btn btn-info']) !!}
					</div>