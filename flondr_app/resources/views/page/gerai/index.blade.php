@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						Gerai SIM
					</h3>                                    
				</div>
				<div class="box-body table-responsive">
					@include('page.partials.notification')
					<div class="col-md-3">
						<div class="row">
						{!! Form::open(array('url' => 'vrs-admin/gerai','method' => 'gerai')) !!}
						<div class="col-md-12">
	              			@include('page.partials.notification')
							@include('page.errors.list')
							@include('page.gerai.form-create',['submitText' => 'Tambahkan'])
						</div>
						{!! Form::close() !!}
						</div>
					</div>
					<div class="col-md-9">
						<table class="table" id="dataTables">
							<thead>
								<tr>
									<th>&nbsp;</th>
									<th>Nama </th>
									<th>Deskripsi </th>
									<th>Alamat</th>
									<th>Peta</th>
								</tr>
							</thead>
							<tbody>
								<?php $i = 1; ?>
								@foreach($data['content'] as $row)
								<tr>
									<td>{{ $i++ }}</td>
									<td>
										{{ $row->title }}
										<div class="action-post-hover">
											<a href="{{route('vrs-admin.gerai.edit',[ $row->id ])}}">edit</a>
											<a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('vrs-admin.gerai.delete',[ $row->id ]) !!}" style="color:red;">trash</a>
										</div>
									</td>
									<td>{{ $row->description }}</td>
	                          		<td>{{ $row->address }}</td>
									<td></td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@include('page.scripts.delete-modal')
@endsection

@section('custom-head')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/media/css/jquery.dataTables.css') }}">
@stop
@section('custom-footer')
<script type="text/javascript" src="{{ asset('plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript">
	$('#dataTables').DataTable();
	$('#dataTables').on('click', '.Switch', function()  {
		var id_post = $(this).data("id");
		setFeatured(id_post);
		$(this).toggleClass('On').toggleClass('Off');
	});
	function setFeatured(id)
	{
		var url = "{{ URL::to('vrs-admin/gerai/featured') }}/" + id +"";
		$.ajax(url)
		.done(function() {
			
		})
		.fail(function() {
			alert( "error" );
		});
	}
</script>
@stop
