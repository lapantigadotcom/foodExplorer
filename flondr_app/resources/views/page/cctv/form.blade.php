<div class="form-group">
	{!! Form::label('ms_media_id','Thumbnail : ') !!}
	<a href="javascript:void(0)" class="btn btn-default" onclick="setFeaturedImage()">Pilih Media</a>
	<div class="col-md-12" id="featuredImage">
	</div>
	{!! Form::hidden('ms_media_id',null, ['class' => 'form-control','id' => 'ms_media_id']) !!}
</div>
<div class="form-group">
	{!! Form::label('title','Title : ') !!}
	{!! Form::text('title',null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>
<div class="form-group">
	{!! Form::label('description','Description : ') !!}
	{!! Form::textarea('description',null, ['class' => 'form-control', 'required' => 'required', 'rows' => 3]) !!}
</div>
<div class="form-group">
	{!! Form::label('embed','Embed Script : ') !!}
	{!! Form::textarea('embed',null, ['class' => 'form-control', 'required' => 'required', 'rows' => 3]) !!}
</div>
<div class="form-group">
	{!! Form::submit($submitText,['class' => 'btn btn-info']) !!}
</div>