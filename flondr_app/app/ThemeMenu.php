<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ThemeMenu extends Model {

	protected $table = 'tr_theme_menus';
	protected $fillable = array('ms_theme_id','code');
	public $timestamps = false;

	public function theme()
	{
		return $this->belongsTo('App\Theme','ms_theme_id','id');
	}

}
