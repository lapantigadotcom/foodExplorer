<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model {

	protected $table = 'tr_logs';
	protected $fillable = array('ms_user_id','name','action','logable_type','logable_id','date');
	public $timestamps = false;

	public function logable()
	{
		return $this->morphTo();
	}
	public function user()
	{
		return $this->belongsTo('\App\User','ms_user_id','id');
	}
}
