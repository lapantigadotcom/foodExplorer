<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Theme extends Model {

	protected $table = 'ms_themes';
	protected $fillable = array('title','description','code','active');
	public $timestamps = true;
	
    public function themeMenu()
    {
        return $this->hasMany('App\ThemeMenu','ms_theme_id','id');
    }

}
