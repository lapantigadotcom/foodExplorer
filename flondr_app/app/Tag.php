<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model {

	protected $table = 'ms_tags';
	protected $fillable = array('title','description','ms_user_id');
	public $timestamps = true;

	public function user()
	{
		return $this->belongsTo('App\User','ms_user_id','id');
	}
	public function post()
    {
        return $this->belongsToMany('App\Post','tr_post_tags','ms_tag_id','ms_post_id');
    }
    public function logs()
    {
        return $this->morphMany('App\Log', 'logable');
    }
}
