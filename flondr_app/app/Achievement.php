<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Achievement extends Model {

	protected $table = 'ms_achievment';
    protected $fillable = array('name', 'description');
	public $timestamps = false;

	public function leader()
	{
        return $this->belongsToMany('App\Leaders','tr_achievment','ms_achievement_id','ms_leaders_id');
        // return $this->belongsToMany('App\Post','tr_category_posts','ms_category_id','ms_post_id');
	}

}
