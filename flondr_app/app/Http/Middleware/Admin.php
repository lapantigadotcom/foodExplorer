<?php namespace App\Http\Middleware;

use Closure;
use Auth;
class Admin {

    public function handle($request, Closure $next)
    {

        if ( Auth::check() && Auth::user()->isAdmin() && Auth::user()->isActive())
        {
            return $next($request);
            //echo "admin";
        }
        //echo "not admin";
        else if ( Auth::check() && Auth::user()->isAdmin() == false && Auth::user()->isActive())
        {
        	return redirect('/profile/'.Auth::user()->id);
        }
        return redirect('/');
    }

}