<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Input;

class DetailPortfolioRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$rules = [
			'ms_media_id' => 'required',
			'ms_portfolio_id' => 'required',
			'caption' => 'required'
		];
		if(Input::get('type') == '1'){
			
		}else{
			$rules['link'] = 'required|url';
		}
		return $rules;
	}

}
