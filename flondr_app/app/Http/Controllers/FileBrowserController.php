<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 */

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Media;
use File;
use Illuminate\Http\Request;
use Storage;

class FileBrowserController extends Controller {

	public function imageList() {
        $imgList = array();

        $files = File::allfiles('./upload/media/');
        
        foreach ($files as $file) {
        	$ar = explode("\\", $file);
        	$name = array_pop($ar);
        	$name_ar = explode("/", $name);
        	$name_last=array_pop($name_ar);
        	$caption = Media::where('file','like',$name_last)->first();
        	if(empty($caption))
        		continue;
        	else
        		$caption = $caption->caption;
            array_push($imgList, array("title"=>$caption==''?'no-caption':$caption, "value"=>asset($name)));
        }
        return json_encode($imgList);
    } 

	public function upload(){
	        $valid_exts = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
	        $max_size = 2000 * 1024; // max file size (200kb)
	        $path = public_path() . '/img/'; // upload directory
	        $fileName = NULL;
	        $file = Input::file('image');
	        // get uploaded file extension
	        //$ext = $file['extension'];
	        $ext = $file->guessClientExtension();
	        // get size
	        $size = $file->getClientSize();
	        // looking for format and size validity
	        $name = $file->getClientOriginalName();
	        if (in_array($ext, $valid_exts) AND $size < $max_size)
	        {
	            // move uploaded file from temp to uploads directory
	            if ($file->move($path,$name))
	            {
	                $status = 'Image successfully uploaded!';
	                $fileName = $name;
	            }
	            else {
	                $status = 'Upload Fail: Unknown error occurred!';
	            }
	        }
	        else {

	            $status = 'Upload Fail: Unsupported file format or It is too large to upload!';
	        }

	        //echo out script that TinyMCE can handle and update the image in the editor

	        return ("
// <![CDATA[
top.$('.mce-btn.mce-open').parent().find('.mce-textbox').val('/img/".$fileName."').closest('.mce-window').find('.mce-primary').click();
// ]]>
"); }

}
