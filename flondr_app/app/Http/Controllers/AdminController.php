<?php namespace App\Http\Controllers;
/*
info@lapantiga.com
*/

use App\Http\Requests\PasswordRequest;
use App\User;
use DB;
use App\Category;
use App\Post;
use App\Achievement;
use App\LeadersAchievement;
use App\Leaders;
use Illuminate\Support\Facades\Hash;
use Session;
use Auth;
// use LaravelAnalytics;

class AdminController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index($page = null)
	{

		$data['activity'] = \App\Log::with('logable','user')->orderBy('id','desc')->paginate(5)->setPath('vrs-admin');
/*
		$data['stat_visitors'] = LaravelAnalytics::getVisitorsAndPageViews(30);
		$data['stat_pages'] = LaravelAnalytics::getMostVisitedPages(30, 20);
		$data['stat_active_users'] = LaravelAnalytics::getActiveUsers();
		$data['stat_keyword'] = LaravelAnalytics::getTopKeywords(30);
		$data['stat_referrers'] = LaravelAnalytics::getTopReferrers(30);
		$data['stat_browser'] = LaravelAnalytics::getTopBrowsers(30);
		*/
		$data['artikeltotal'] = DB::table('ms_posts')->count();
		$data['produktotal'] = DB::table('ms_leaders')->count();
		$data['gambartotal'] = DB::table('ms_medias')->count();
		$data['menutotal'] = DB::table('tr_menu_details')->count();
		$data['artikelbaru']= DB::table('ms_posts')
              ->select('title',
                'description',
                'created_at',
                'updated_at',
                'meta_description')
              ->orderBy('updated_at', 'desc')
              ->take(3)->get();
  		$data['artikellist'] = \App\Post::with('user','category','tag')->orderBy('updated_at','desc')->take(5)->get();
		$data['content'] = Leaders::orderBy('id','asc')->take(5)->get();
		return view('page.index',compact('data'));

	}

	public function password()
	{
		return view('page.dashboard.password',compact('data'));
	}
	public function Changepassword(PasswordRequest $request)
	{
		$data = User::find(Auth::user()->id);
		$newP = $request->input('password');
		$oldP = $request->input('oldpassword');
		if(Hash::check($oldP, $data->password))
		{
			$data->password = Hash::make($newP);
			$data->save();
			Session::flash('success','Password berhasil diganti');			
			//echo "go";
		}else{
			Session::flash('error','Password gagal diganti, password lama tidak cocok');
			//echo "no";
		}
		//echo $oldP;
		return redirect()->back();
	}

}
