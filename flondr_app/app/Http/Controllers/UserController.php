<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 
*/

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Requests\MitraRequest;
use Session;
use App\User;
use App\UserDetail;
use App\Media;
use App\Gender;
use App\City;

class UserController extends BasicController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->updateAllCoordMitra();
		$this->fillCustom();
		$data['content'] = User::where('ms_privilige_id', '2')->get();
		return view('page.user.index')->with('data',$data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['gender'] = Gender::all();
		$data['city'] = City::all();
		return view('page.user.create')->with('data',$data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(MitraRequest $request)
	{
		//
		if(User::where('email', $request->email)->get()->first() == null)
		{
			$data = User::create($request->all());
			$data->ms_privilige_id = 2;
			$data->password = \Hash::make($data->password);

			if($request->latitude == "" || $request->longitude == "")
			{
				$cityName = City::where('id', $request->ms_city_id)->first()->name;
				$tmpCoord = $this->getCoordinatesFromMitra($request->address, $cityName);
				if($tmpCoord == 'null')
				{
					$tmpCoord = $this->getCoordinates($request->address.", ".$cityName);
					if($tmpCoord == 'null')
					{
						$tmpCoord = $this->getCoordinates($cityName);
						if($tmpCoord == 'null')
							$tmpCoord = "0,0";
					}
				}
	    		$tmpCoord = explode(',', $tmpCoord);
	    		$request->latitude = $tmpCoord[0];
	    		$request->longitude = $tmpCoord[1];
			}

			$userdetail = UserDetail::create(array(
				'panggilan' => $request->panggilan,
				'custom' => $request->custom,
				'notelp' => $request->notelp,
				'nohp' => $request->nohp,
				'address' => $request->address,
				'kodepos' => $request->kodepos,
				'facebook' => $request->facebook,
				'twitter' => $request->twitter,
				'latitude' => $request->latitude,
				'longitude' => $request->longitude,
				'pin_bb' => $request->pin_bb,
				'sn_mesin' => $request->sn_mesin,
				'jenis_mesin' => $request->jenis_mesin,
				'ms_media_id' => $request->ms_media_id,
				'ms_gender_id' => $request->ms_gender_id,
				'ms_city_id' => $request->ms_city_id
				));
			$data->tr_user_detail_id = $userdetail->id;

			$userdetail->save();
			$data->save();
			//echo $foto->file;

			// $log = array();
			// $log['action'] = 'insert';
			// $log['name'] = $request->input('name');
			// $data->logs()->save($this->log($log));

			$data['content'] = User::where('ms_privilige_id', '2')->get();
			Session::flash('success','Registrasi berhasil');
			return view('page.user.index')->with('data',$data);
		}
		$data['gender'] = Gender::all();
		$data['city'] = City::all();
		Session::flash('error','Email sudah digunakan');
		return view('page.user.create')->with('data',$data);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content'] = User::all()->find($id);
		$data['content_detail'] = $data['content']->detail()->first();
		$data['content_detail']['name'] = $data['content']->name;
		$data['content_detail']['email'] = $data['content']->email;
		$data['content_detail']['password'] = $data['content']->password;
			
		$data['gender'] = Gender::all();
		$data['city'] = City::all();
		return view('page.user.edit',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(MitraRequest $request, $id)
	{
		if(User::where('id', $id)->get()->first()->email == $request->email || User::where('email', $request->email)->get()->first() == null)
		{
			$data = User::find($id);
			$userdetail = UserDetail::find($data->tr_user_detail_id);
			$data->update(array(
				'email' => $request->email,
				'name' => $request->name
				));
			
			if($request->latitude == "" || $request->longitude == "" || 
				$userdetail->address != $request->address || 
				$userdetail->ms_city_id != $request->ms_city_id ||
				$userdetail->latitude == "0" || $userdetail->longitude == "0")
			{
				$cityName = City::where('id', $request->ms_city_id)->first()->name;
				$tmpCoord = $this->getCoordinatesFromMitra($request->address, $cityName);
				if($tmpCoord == 'null')
				{
					$tmpCoord = $this->getCoordinates($request->address.", ".$cityName);
					if($tmpCoord == 'null')
					{
						$tmpCoord = $this->getCoordinates($cityName);
						if($tmpCoord == 'null')
							$tmpCoord = "0,0";
					}
				}
	    		$tmpCoord = explode(',', $tmpCoord);
	    		$request->latitude = $tmpCoord[0];
	    		$request->longitude = $tmpCoord[1];
			}

			$userdetail->update(array(
				'enagic_id' => $request->enagic_id,
				'panggilan' => $request->panggilan,
				'custom' => $request->custom,
				'notelp' => $request->notelp,
				'nohp' => $request->nohp,
				'address' => $request->address,
				'kodepos' => $request->kodepos,
				'latitude' => $request->latitude,
				'longitude' => $request->longitude,
				'facebook' => $request->facebook,
				'twitter' => $request->twitter,
				'pin_bb' => $request->pin_bb,
				'sn_mesin' => $request->sn_mesin,
				'jenis_mesin' => $request->jenis_mesin,
				'ms_city_id' => $request->ms_city_id,
				'ms_gender_id' => $request->ms_gender_id
				));
			if($request->password != "")
				$data->update(array(
					'password' => \Hash::make($request->password)
					));
			if($request->ms_media_id != "")
				$userdetail->update(array(
					'ms_media_id' => $request->ms_media_id
					));
			Session::flash('success','Data berhasil diperbarui');
			return redirect()->route('vrs-admin.user.index');
		}
		//echo $request->ms_media_id;
		// $log = array();
		// $log['action'] = 'update';
		// $log['name'] = $data->title;
		// $data->logs()->save($this->log($log));

		$data['content'] = User::all()->find($id);
		$data['content_detail'] = $data['content']->detail()->first();
		$data['content_detail']['name'] = $data['content']->name;
		$data['content_detail']['email'] = $data['content']->email;
		$data['content_detail']['password'] = $data['content']->password;
			
		$data['gender'] = Gender::all();
		$data['city'] = City::all();
		Session::flash('error','Email sudah digunakan');
		return view('page.user.edit',compact('data'));
		// $this->edit($id);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data = User::find($id);
		$userDetail = UserDetail::find($data->tr_user_detail_id);
		//$foto = Media::find($userDetail->ms_media_id);

		// $log = array();
		// $log['action'] = 'delete';
		// $log['name'] = $data->title;
		// $data->logs()->save($this->log($log));

		$data->delete();
		$userDetail->delete();
		//$foto->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('vrs-admin.user.index');
	}

	public function featured($id)
	{
		$data = User::find($id);
		if(empty($data))
		{
			return;
		}
		if($data->active == '1')
		{
			$data->active = '0';
		}else{
			$data->active = '1';
		}
		$data->save();
		return;
	}

	function getCoordinatesFromMitra($address, $city)
	{
		$coord = $this->getCoordinates($address);
		if($coord == 'null')
		{
			$coord = $this->getCoordinates($address.", ".$city);
			if($coord == 'null')
			{
				$coord = $this->getCoordinates($city);
				if($coord == 'null')
					$coord = 'null';
			}
		}
		//echo $coord."<br>";
		return $coord;
	}

	function getCoordinates($address) 
	{ 
		$key1 = "AIzaSyDenG9f8NSuPaEkFoxpMpHZrs3DyMHsBSk";
		$key2 = "AIzaSyAbcKfXVS-7CROSC5Jafzv5yjEzbqtuIno";
		$key3 = "AIzaSyDcvXhVNEmgLApyxwQl5QXWKTZj-uQveOg";
		$key = $key1;
		//echo $address."<br>";
		$address = str_replace(" ", "+", $address); // replace all the white space with "+" sign to match with google search pattern		 
		$url = "https://maps.google.com/maps/api/geocode/json?key=$key&sensor=false&address=$address";		 
		//echo $url."<br>";
		$response = file_get_contents($url);		 
		$json = json_decode($response,TRUE); //generate array object from the response from the web	
		//echo $response."<br>";
		if(count ($json['results'])	> 0)
			return ($json['results'][0]['geometry']['location']['lat'].",".$json['results'][0]['geometry']['location']['lng']);	 		
		return 'null';
	}

	function updateAllCoordMitra()
	{
		$allUser = User::where('ms_privilige_id', '2')->get();
		foreach ($allUser as $row) 
		{
			if($row->detail->latitude == "" || $row->detail->longitude == "" || $row->detail->latitude == "0" || $row->detail->longitude == "0")
			{
				$tmpCoord = "0,0";
				$tmpCoord = $this->getCoordinatesFromMitra($row->detail->address, $row->detail->city->name);
				if($tmpCoord == 'null')
				{
					$tmpCoord = $this->getCoordinates($row->detail->address.", ".$row->detail->city->name);
					if($tmpCoord == 'null')
					{
						$tmpCoord = $this->getCoordinates($row->detail->city->name);
						if($tmpCoord == 'null')
							$tmpCoord = "0,0";
					}
				}
	    		//echo $tmpCoord."<br>";
	    		$tmpCoord = explode(',', $tmpCoord);
	    		$latitude = $tmpCoord[0];
	    		$longitude = $tmpCoord[1];

				$row->detail->update(array(
					'latitude' => $latitude,
					'longitude' => $longitude
					));
	    		$row->detail->save();
	    		// echo $row->detail->latitude.",";
	    		// echo $row->detail->longitude."<br>";
			}
		}
	}

	function fillCustom()
	{
		$allUser = User::where('ms_privilige_id', '2')->get();
		foreach ($allUser as $row) 
		{
			if($row->detail->custom == "")
			{
				$row->detail->update(array(
					'custom' => $row->detail->panggilan
					));
	    		$row->detail->save();
			}
		}
	}

}
