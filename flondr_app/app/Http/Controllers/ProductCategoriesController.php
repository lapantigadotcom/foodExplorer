<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 
*/

use App\Category;
use App\Http\Controllers\Controller;
use App\ProductCategories;

class ProductCategoriesController extends BasicController {

	public function __construct() {
		$this->modelName = 'App\ProductCategories';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index()
	{
		$data['content'] = ProductCategories::all();
		return view('page.product-category.index')->with('data',$data);
	}
}