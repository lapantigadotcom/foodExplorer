<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 
*/
//require_once 'vendor/autoload.php';

use UrbanAirship\Airship;
use UrbanAirship\AirshipException;
use UrbanAirship\UALog;
use UrbanAirship\Push as P;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

use Illuminate\Http\Request;
use App\Http\Requests\NotificationRequest;
use App\User;
use App\Notification;
use Illuminate\Support\Facades\Hash;
use Session;
use Auth;
use LaravelAnalytics;

class NotificationController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('page.notification.create');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['notif'] = Notification::orderBy('created_at','desc')->get();
		return view('page.notification.create')->with('data', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(NotificationRequest $request)
	{
		// #urbanairship
		UALog::setLogHandlers(array(new StreamHandler("php://stdout", Logger::DEBUG)));

		$airship = new Airship("xxJt1ca5RUeuztS406klWQ", "4BKNccQJRle7NKA3rMbZ5Q");

		try {
		    $response = $airship->push()
		        ->setAudience(P\all)
		        ->setNotification(P\notification($request->message))
		        ->setDeviceTypes(P\all)
		        ->send();
		} catch (AirshipException $e) {
		    print_r($e);
		}

		// save message
		$notification = Notification::create(array(
			'title' => $request->title,
			'description' => $request->message,
			));
		$notification->save();

		Session::flash('success','Pesan terkirim');
		return redirect()->route('vrs-admin.notification.create');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

	}
	public function enabled($id=null)
	{

	}

}
