<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 
*/

use App\Http\Controllers\BasicController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\EventRequest;
use App\Event;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Storage;
use Session;
use Image;
use Input;
use DateTime;

class EventController extends BasicController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = Event::all();
		return view('page.event.index')->with('data',$data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	public function create()
	{
		return view('page.event.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(EventRequest $request)
	{
		$data=Event::create(array(
			'name' => $request->name,
			'description' => $request->description,
			'date' => date('Y-m-d', strtotime($request->date))
			));

		Session::flash('success','Data berhasil ditambahkan');
		return redirect()->route('vrs-admin.event.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		echo "string";
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content'] = Event::all()->find($id);
		return view('page.event.edit',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(EventRequest $request,$id)
	{
		$data=Event::find($id);
		$date='';
		if(strpos($request->date, '/') !== false)
			$date=explode("/",$request->date);
		else if(strpos($request->date, '-') !== false)
			$date=explode("-",$request->date);
		//echo $request->date."<br>";
		//echo date('Y-m-d', strtotime($request->date));
		$date = date('Y-m-d', strtotime($date[1]."/".$date[0]."/".$date[2]));
		// //echo $date;
		$data->update(array(
			'name' => $request->name,
			'description' => $request->description,
			'date' => $date
			));

		Session::flash('success','Data berhasil diperbarui');
		return redirect()->route('vrs-admin.event.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data=Event::find($id);
		$data->delete();		

		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('vrs-admin.event.index');
	}

	public function getAllAjax()
	{
		$data = Event::all();
		return response()->json($data);
	}
}
